# Pages statiques du site https://code.gouv2.fr

Ces pages sont générées par le script [gencodegouv](https://git.framasoft.org/etalab/gencodegouv).

## Licence d'utilisation

Les pages de ce site web sont publiées sous [licence ouverte](https://www.etalab.gouv.fr/licence-ouverte-open-licence).
